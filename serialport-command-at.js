const serialPort = require('serialport');
const Readline = require('@serialport/parser-readline')
var readline = require('readline');
const { parse } = require('path');
var util = require('util');


const chalk = require('chalk');
const log = console.log;

// 65

const port = new serialPort(
    'COM113',
    { baudRate: 115200 }
)

const parser = new serialPort.parsers.Readline()
port.pipe(parser)
var rl = readline.createInterface(process.stdin, process.stdout);

let menuCommandAT = [
    "Setear APN de Claro",
    "Setear APN de Hologram",
    "Preguntar APN",
    "Intensidad de Sennal",
    "Operador de Red",
    "GPRS Network",
    "Check Pin",
    "Tipo de Tecnologia",
    "Bandas de Frecuencia",
    "Ativa GPRS coneccion",
    "Chequea IP Dinamica",
    "Chequa DNS Primario",
    "Modo Avion",
    "Modo Normal",
    "Activar PDP",
    "Tipo de Tecnologia Extendida",
    "Lista de Codigos de Operadores",
    "Caracteres Permitidos",
    "Bandas de trabajo"
];

console.log(chalk.green("   Digite un numero"));
printMenu();


rl.on('line', (input) => {
    if (input.trim() === 'S') {
        console.log('Chao');
        process.exit();
    }

    switch (input.trim()) {
        case '01':
            port.write('AT+CGDCONT=1,"IP","ba.amx"\r')
            break;
        case '02':
            port.write('AT+CGDCONT=1,"IP","hologram"\r')
            break;
        case '03':
            port.write('AT+CGDCONT?\r')
            break;
        case '04':
            port.write('AT+CSQ\r')
            break;
        case '05':
            port.write('AT+COPS?\r')
            break;
        case '06':
            port.write('AT+CGREG?\r')
            break;
        case '07':
            port.write('AT+CPIN?\r')
            break;
        case '08':
            port.write('AT+URAT?\r')
            break;
        case '09':
            port.write('AT+UBANDSEL?\r')
            break;
        case '10':
            port.write('AT+UPSDA=0,3\r')
            break;
        case '11':
            port.write('AT+UPSND=0,0\r')
            break;
        case '12':
            port.write('AT+CGCONTRDP\r')
            break;
        case '13':
            port.write('AT+CFUN=4\r')
            break;
        case '14':
            port.write('AT+CFUN=1\r')
            break;
        case '15':
            port.write('AT+CGACT=1,1\r')
            break;
        case '16':
            port.write('AT+URAT=?\r')
            break;
        case '17':
            port.write('AT+COPN\r')
            break;
        case '18':
            port.write('AT+CSCS=?\r')
            break;
        case '19':
            port.write('AT+UBANDSEL=?\r')
            break;

        default:
            console.log('Lo lamento, no existe el comando ' + input.trim() + '.');
    }

    console.log(" ");
    console.log(chalk.green.bold(". - Proceso de Comando AT - ."));
    console.log(chalk.green("   Digite un numero"));
    printMenu();
    console.log(" ");



});

parser.on('data', (atCommand) => {
    log(chalk.blue('SARA-U201: ') + atCommand);
})

function printMenu() {
    menuCommandAT.forEach(function (elemento, indice, array) {
        if ((indice + 1) <= 9)
            process.stdout.write(chalk.green("0"));
        log(chalk.green(indice + 1) + " " + elemento);
    })
}



